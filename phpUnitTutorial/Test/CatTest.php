<?php

namespace phpUnitTutorial\Test;
use PHPUnit\Framework\TestCase;
use phpUnitTutorial\Cat;
use phpUnitTutorial\Client;

class CatTest extends TestCase{
	/**
	 *  @group Cat
	 */ 
	public function testMeow(){
		$mockClient = $this->getMockBuilder('phpUnitTutorial\Client')->getMock();
		$mockClient->expects($this->once())->method('get')->willReturn('response');
		$c = new Cat($mockClient);
		$c->meow();
		
	}
	/**
	 * @group Cat
	 */ 
	public function testClient(){
		$client = new Client();
		$client->get();
		$this->assertTrue(true);
	}
}